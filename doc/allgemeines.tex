\part{Allgemeines}

\section{Allgemeines zum Paket}

\subsection{Wichtiger Hinweis zur neuen Version}
	Das Schule-Paket wurde vollständig überarbeitet. Diese
	Version enthält grundlegende, strukturelle Veränderungen.
	So wird unter anderem die Vielzahl an Dokumentenklassen
	stark reduziert und die Konfiguration erfolgt nun über
	Paketoptionen.

	Dies führt zu großen Veränderungen der Schnittstelle. Die
	neue Version ist damit \textbf{nicht kompatibel} zu allen
	vorhergehenden Versionen. Es besteht allerdings ein
	\textbf{Kompatibilitätsmodus}, der automatisch für alle alten
	Dokumentenklassen aktiv ist. Alte Dokumente lassen sich somit
	weiterhin setzen, die Schnittstelle wird aber nicht
	weiterentwickelt. Bestehende Fehler werden in der alten
	Version nicht behoben.

	Diese Änderungen ermöglichen die Lösung einiger bestehender
	Probleme (u.\,a. Quelltexte in Aufgaben und Lösungen).
	Zusätzlich wurde die Nutzung des Pakets vereinheitlicht und
	die Nutzung in anderen Dokumentenklassen ermöglicht, sodass
	etwa die Aufgabenumgebungen auch in Beamer-Präsentationen
	übernommen werden können. Klausuren unterstützen nun die
	automatische Erzeugung von Erwartungshorizonten.

	Eine weitere große Veränderung ist die Ausgliederung der
	ausbildungsrelevanten Teile (Unterrichtsbesuche,
	Stundenverläufe etc.) des Pakets. In der
	Vergangenheit hat sich gezeigt, dass sich die Anforderungen der
	verschiedenen, an der Lehrerbildung beteiligten Stellen
	stark voneinander unterscheiden.
	Daher werden die entsprechenden Funktionen des Pakets
	ausgegliedert, sodass sie einfach in eigenen Dokumenten
	genutzt werden können. Die bestehenden Vorlagen werden
	als eigenständige Klassen mitgeliefert.

\subsection{Manuelle Installation}
	Um die Pakete und Klassen nutzen zu können, gibt es drei
	Varianten. In der folgenden Beschreibung dieser Möglichkeiten
	wird von einer standardisierten \LaTeX-Installation
	ausgegangen -- weitere Hinweise können der Dokumentation der
	jeweiligen \TeX-Distribution entnommen werden:

\begin{description}
\item[Global]
	Für die globale/systemweite Installation der Pakete und
	Klassen	müssen diese in das globale \LaTeX-Verzeichnis der
	\TeX-Installation kopiert werden: unter Linux in der Regel
	\texttt{/usr/share/texmf/tex/latex/}. In diesem kann ein
	weiteres Verzeichnis wie z.\,B. \texttt{schule} angelegt
	werden, in das alle Dateien des Schulepakets
	kopiert werden.

	Damit die Quellen anschließend dem System bekannt sind, muss
	der Cache von \LaTeX{} neu aufgebaut werden. Bei den meisten
	Linux-Installationen geschieht dieses durch den Aufruf von
	\texttt{texhash}.

\item[Benutzer]
	Damit ein Nutzer auf die Quellen zugreifen kann,
	müssen diese im Benutzerverzeichnis (Home directory)
	abgelegt werden. Dies geschieht durch das Kopieren der
	Pakete und Klassen in das Verzeichnis
	\texttt{texmf/tex/latex/} im Benutzerverzeichnis, das ggf.
	erst angelegt werden muss. Auch hier sollte -- wie bei der
	globalen Installation -- ein eigenes Unterverzeichnis
	angelegt werden.

\item[Lokal]
	Um die Klassen und Pakete ohne weitere Installation
	nutzen zu können, ist es darüber hinaus möglich, die
	benötigten Dateien in das Verzeichnis zu kopieren, in dem
	die Datei liegt, die übersetzt werden soll. Dies ist jedoch
	aufgrund des Umfangs des Schulepakets weniger empfehlenswert.
\end{description}

\subsubsection{Voraussetzungen}
Ein Grund für die Nutzung des Schule-Pakets und der damit
verbundenen speziellen Klassen und Pakete liegt
darin, viele der häufig  benötigten Pakete zusammen zu fassen.
Daher müssen diese für die Benutzung vorhanden sein. Die meisten
sind Standardpakete, die mit jeder normalen Installation
mitgeliefert sind.
Es folgt eine Aufstellung der Voraussetzungen
für das Paket \pkg{schule} und die vorhandenen Module.
Mit einem Stern (*) markierte Pakete sind im Paket
\pkg{schule} bereits enthalten:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{amsmath}
		\item \pkg{babel}
		\item \pkg{environ}
		\item \pkg{fontenc}
		\item \pkg{forarray}
		\item \pkg{graphicx}
		\item \pkg{hyperref}
		\item \pkg{ifthen}
		\item \pkg{pgfopts}
		\item \pkg{schulealt} *
		\item \pkg{tikz}
		\item \pkg{xcolor}
		\item \pkg{xparse}
		\item \pkg{xstring}
		\item \pkg{zref-totpages}
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Fach \enquote{Informatik} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{listings}
		\item \pkg{pgf-umlcd}
		\item \pkg{pgf-umlsd}
		\item \pkg{relaycircuit} *
		\item \pkg{struktex}
		\item \pkg{syntaxdi}
	\end{smallitemize}
\end{multicols}

Folgende TikZ-Bibliotheken werden für das Fach \enquote{Informatik} benötigt:
\begin{smallitemize}
	\item er
\end{smallitemize}

Folgende Pakete werden zusätzlich für das Fach \enquote{Physik} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
	\item \pkg{circuittikz}
	\item \pkg{units}
	\item \pkg{mhchem}
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Fach \enquote{Geschichte} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{uni-wtal-ger}
		\item \pkg{marginnote}
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Modul \enquote{Aufgaben} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{xsim}
		\item \pkg{utfsym} *
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Modul \enquote{Format} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{amssymb}
		\item \pkg{array}
		\item \pkg{colortbl}
		\item \pkg{csquotes}
		\item \pkg{ctable}
		\item \pkg{enumitem}
		\item \pkg{eurosym}
		\item \pkg{graphicx}
		\item \pkg{longtable}
		\item \pkg{multicol}
		\item \pkg{multirow}
		\item \pkg{setspace}
		\item \pkg{tikz}
		\item \pkg{ulem}
		\item \pkg{utfsym} *
		\item \pkg{xspace}
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Modul \enquote{Symbole} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{utfsym} *
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Modul \enquote{Texte} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item \pkg{lineno}
		\item \pkg{multicol}
	\end{smallitemize}
\end{multicols}

Folgende Pakete werden zusätzlich für das Modul \enquote{Texte} benötigt:
\begin{multicols}{4}
    \begin{smallitemize}
        \item \pkg{standalone}
    \end{smallitemize}
\end{multicols}


Folgende TikZ-Bibliotheken werden für das Zusatzpaket \pkg{relaycircuit} benötigt:
\begin{multicols}{4}
	\begin{smallitemize}
		\item arrows
		\item scopes
		\item shadows
		\item shapes.misc
	\end{smallitemize}
\end{multicols}

\subsection{Begriffsklärungen}
	\label{sec:begriffe}
	\begin{description}
		\item[Zusatzpaket]
			Das Paket \pkg{schule} liefert einige \LaTeX-Pakete mit,
			die für das Paket entwickelt wurden, aber von diesem
			unabhängig nutzbar sind.

			Diese Pakete werden im Folgenden als Zusatzpaket
			bezeichnet.

		\item[Modul]
			Im Gegensatz zu einem Zusatzpaket ist ein Modul
			enger mit dem Hauptpaket verzahnt. Es lässt sich nicht
			unabhängig von diesem nutzen.

			Module bestehen aus einer oder mehreren
			\LaTeX-Quelldateien, die in das Paket eingebunden werden.

			Siehe auch die Beschreibung in der
			Entwicklungsdokumentation im
			\prettyref{sec:devmoduleModul}.

		\item[Fachmodul]
			Ein Fachmodul ist ähnlich aufgebaut wie ein	normales
			Modul für das Schulepaket, wird allerdings für
			fachspezifische Erweiterungen genutzt und erfüllt somit
			einen anderen Zweck.

			Siehe auch die Beschreibung in der
			Entwicklungsdokumentation im
			\prettyref{sec:devmoduleFachmodul}.

		\item[Dokumenttyp]
			Ein Dokumenttyp ist ähnlich aufgebaut wie ein normales
			Modul für das Schulepaket, wird allerdings für
			typspezifische Erweiterungen genutzt und erfüllt somit
			einen anderen Zweck.

			Siehe auch die Beschreibung in der
			Entwicklungsdokumentation im
			\prettyref{sec:devDokumenttyp}.

	\end{description}

\subsection{Arten der Nutzung}
	\subsubsection{Nutzung für Dokumente}
		Wenn zumindest ein \option{typ} in den Paketoptionen angegeben
		wird, werden viele Module und mit diesen auch viele externe
		Pakete geladen und konfiguriert, von denen einige auch die
		grundlegende Struktur der zu setzenden Dokumente verändern.

		Außerdem werden Entscheidungen für das Aussehen der Dokumente
		getroffen. Man hat hier noch viele Freiheiten, ist jedoch auf
		die grundlegenden Vorgaben des Schule-Pakets festgelegt.

		Dies kann auch zu Inkompatibilitäten mit bestimmten
		Dokumentenklassen oder externen Paketen führen, \zB\space
		könnten Option-Clashes auftreten.

	\subsubsection{Eingebettete Nutzung}
		Es trat immer wieder der Wunsch auf, dass Funktionen aus dem
		Schulepaket auch in anderen Dokumenten oder gar in
		Dokumentenklassen oder anderen Paketen nutzen zu können.

		Das war aus den oben genannten Gründen schwierig. Inzwischen
		ist dies möglich, in dem man beim Laden des Pakets die Option
		\keyis{typ}{ohne} angibt.

		Damit wird das Paket in einen \enquote{minimalinvasiven} Modus
		geschaltet, der nur die nötigsten Module lädt und so wenig
		Vorgaben macht wie möglich.

		Weitere Module können dann natürlich geladen werden.

	\subsubsection{Nutzung über die Dokumentenklassen}
		Die Nutzungsvariante mit den wenigsten Freiheiten ist die
		über eine der Dokumentenklassen. Anpassungen sind hier nur
		sehr eingeschränkt möglich und es werden sehr viele Vorgaben
		gemacht. Sie ist allerdings gleichzeitig die Variante, bei
		der man am wenigsten konfigurieren und eigene Einstellungen
		vornehmen muss. Siehe auch \prettyref{subsubsec:paketoptionen}.

\subsection{Kompilieren der Dokumente}
	Das Schulepaket ist für die Nutzung von \texttt{pdflatex}
	optimiert und wurde nur damit getestet.

	Aufgrund des komplexen Aufbaus kann es besonders bei der Nutzung
	des Moduls \module{Aufgaben} notwendig sein, mindestens zwei
	Läufe von \texttt{pdflatex} durchzuführen. Dies liegt daran, das
	eine Menge Zwischendateien mit Punkten und anderen Metadaten zu
	den Aufgaben erstellt werden müssen und dann damit Berechnungen
	durchgeführt werden.

	Es kann deshalb passieren, dass nach der Änderung der Anzahl der
	Aufgaben der erste Durchlauf mit sehr vielen Fehlern fehlschlägt.
